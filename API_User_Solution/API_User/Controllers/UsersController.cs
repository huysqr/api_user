﻿using API_User.Models.Domain;
using API_User.Models.DTO;
using API_User.Repositories.IRepositories;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace API_User.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserRepository userRepository;
        private readonly IMapper mapper;
        private readonly ILogger<UsersController> logger;

        public UsersController(IUserRepository userRepository, IMapper mapper, ILogger<UsersController> logger)
        {
            this.userRepository = userRepository;
            this.mapper = mapper;
            this.logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] string? filterOn, [FromQuery] string? filterQuery,
            [FromQuery] string? sortBy, [FromQuery] bool? isAscending,
            [FromQuery] int pageNumber = 1, [FromQuery] int pageSize = 1000)
        {
            var users = await userRepository.GetAllAsync(filterOn, filterQuery, sortBy, isAscending ?? true, pageNumber, pageSize);
            
            var usersDTO = mapper.Map<List<UserDTO>>(users);
            
            return Ok(usersDTO);
        }

        //POST: /api//Auth/Register
        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Register([FromBody] CreateUserDTO userDTO)
        {

            var user = mapper.Map<User>(userDTO);
            var identityResult = await userRepository.CreateAsync(user, userDTO.Password);
            if (identityResult == null)
            {
                return BadRequest("Something went wrong");

            }
            return Ok("Users was registered! Please login.");


        }

        [HttpGet]
        [Route("{id:Guid}")]
        public async Task<IActionResult> GetByUserName([FromRoute] Guid id)
        {

            var user = await userRepository.GetByUserIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            var userDTO = mapper.Map<UserDTO>(user);
            return Ok(userDTO);
        }

        [HttpPut]
        [Route("{id:Guid}")]
        public async Task<IActionResult> Update([FromRoute] Guid id, [FromBody] UpdateUserDTO updateUserDTO)
        {
            var user = mapper.Map<User>(updateUserDTO);
            var userUpdate = await userRepository.UpdateAsync(id, user);

            if (userUpdate == null)
            {
                return BadRequest();
            }
            var userResult = mapper.Map<UserDTO>(userUpdate);
            return Ok(userResult);
        }

        [HttpDelete]
        [Route("{id:Guid}")]
        public async Task<IActionResult> Delete([FromRoute] Guid id)
        {
            var user = await userRepository.DeleteAsync(id);
            if (user == null)
            {
                return BadRequest();
            }
            var walkDTO = mapper.Map<User>(user);
            return Ok(walkDTO);
        }

    }
}
