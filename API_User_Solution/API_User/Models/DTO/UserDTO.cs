﻿using System.ComponentModel.DataAnnotations;

namespace API_User.Models.DTO
{
    public class UserDTO
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

        [MaxLength(200, ErrorMessage = "Code has to be a minimum of 200 characters")]
        public string? FullName { get; set; }
        [MaxLength(300, ErrorMessage = "Code has to be a minimum of 300 characters")]
        public string? Address { get; set; }
        [MaxLength(10, ErrorMessage = "Code has to be a minimum of 10 characters")]
        public string PostCode { get; set; }
        //public string[] Roles { get; set; }
        
    }

    public class CreateUserDTO
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public string? Email { get; set; }
        public string? PhoneNumber { get; set; }

        [MaxLength(200, ErrorMessage = "Code has to be a minimum of 200 characters")]
        public string? FullName { get; set; }
        [MaxLength(300, ErrorMessage = "Code has to be a minimum of 300 characters")]
        public string? Address { get; set; }
        [MaxLength(10, ErrorMessage = "Code has to be a minimum of 10 characters")]
        public string? PostCode { get; set; }
        //public string[] Roles { get; set; }
    }

    public class UpdateUserDTO
    {
        //public string Password { get; set; }
        public string? Email { get; set; }
        public string? PhoneNumber { get; set; }

        [MaxLength(200, ErrorMessage = "Code has to be a minimum of 200 characters")]
        public string? FullName { get; set; }
        [MaxLength(300, ErrorMessage = "Code has to be a minimum of 300 characters")]
        public string? Address { get; set; }
        [MaxLength(10, ErrorMessage = "Code has to be a minimum of 10 characters")]
        public string? PostCode { get; set; }
    }
}
