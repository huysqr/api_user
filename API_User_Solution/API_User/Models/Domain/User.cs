﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace API_User.Models.Domain
{
    public class User : IdentityUser
    {
        [MaxLength(200)]
        public string? FullName { get; set; }
        [MaxLength(300)]
        public string? Address { get; set; }
        [MaxLength(10)]
        public string? PostCode { get; set; }
    }
}
