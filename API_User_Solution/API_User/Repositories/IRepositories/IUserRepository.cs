﻿using API_User.Models.Domain;
using Microsoft.AspNetCore.Identity;

namespace API_User.Repositories.IRepositories
{
    public interface IUserRepository
    {
        Task<List<User>> GetAllAsync(string? filterOn = null, string? filterQuery = null,
                                    string? sortBy = null, bool isAscending = true,
                                    int pageNumber = 1, int pageSize = 1000);
        Task<User?> GetByUserIdAsync(Guid id);
        Task<User> CreateAsync(User user, string password);
        Task<User?> UpdateAsync(Guid id, User user);
        Task<User?> DeleteAsync(Guid id);
    }
}
