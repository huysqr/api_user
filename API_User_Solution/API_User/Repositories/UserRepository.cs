﻿using API_User.Models.Domain;
using API_User.Repositories.IRepositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace API_User.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly UserManager<User> userManager;

        public UserRepository(UserManager<User> userContext)
        {
            this.userManager = userContext;
        }

        public async Task<User> CreateAsync(User user, string password)
        {
            //Init Role 
            List<string> stringList = new List<string>
            {
                "User"
            };
            IEnumerable<string> enumerableStrings = stringList;

            var identityResult = await userManager.CreateAsync(user, password);
            if (identityResult.Succeeded)
            {
                //Add roles to this user

                identityResult = await userManager.AddToRolesAsync(user, enumerableStrings);

                if (identityResult.Succeeded)
                {
                    return await userManager.Users.FirstOrDefaultAsync(i => i.UserName == user.UserName);
                }

            }
            return null;
        }

        public async Task<User?> DeleteAsync(Guid id)
        {
            var checkUserExist = await userManager.Users.FirstOrDefaultAsync(u => u.Id == id.ToString());
            if (checkUserExist == null)
            {
                return null;
            }
            var userDele = await userManager.DeleteAsync(checkUserExist);
            if (!userDele.Succeeded)
            {
                return null;
            }
            return checkUserExist;
        }

        public async Task<List<User>> GetAllAsync(string? filterOn = null, string? filterQuery = null, string? sortBy = null, bool isAscending = true, int pageNumber = 1, int pageSize = 1000)
        {
            var users = userManager.Users.AsQueryable();

            //Filtering
            if (string.IsNullOrWhiteSpace(filterOn) == false && string.IsNullOrWhiteSpace(filterQuery) == false)
            {
                if (filterOn.Equals("UserName", StringComparison.OrdinalIgnoreCase))
                {
                    users = users.Where(u => u.UserName.Contains(filterQuery));
                }
            }

            //Sorting
            if (string.IsNullOrWhiteSpace(sortBy) == false)
            {
                if (sortBy.Equals("UserName", StringComparison.OrdinalIgnoreCase))
                {
                    users = isAscending ? users.OrderBy(x => x.UserName) : users.OrderByDescending(x => x.UserName);
                }
                else if (sortBy.Equals("PhoneNumber", StringComparison.OrdinalIgnoreCase))
                {
                    users = isAscending ? users.OrderBy(x => x.PhoneNumber) : users.OrderByDescending(x => x.PhoneNumber);
                }
            }

            //Pagination
            var skipResult = (pageNumber - 1) * pageSize;
            var result = await users.Skip(skipResult).Take(pageSize).ToListAsync();
            return result;
        }

        public async Task<User?> GetByUserIdAsync(Guid id)
        {
            var user = await userManager.Users.FirstOrDefaultAsync(u => u.Id == id.ToString());
            if (user == null) 
            { 
                return null; 
            }
            return user;
        }

        public async Task<User?> UpdateAsync(Guid id, User user)
        {
            var updateUser = await userManager.Users.FirstOrDefaultAsync(u => u.Id == id.ToString());
            if (updateUser == null)
            {
                return null;
            }

            updateUser.Email = user.Email;
            updateUser.PhoneNumber = user.PhoneNumber;
            updateUser.FullName = user.FullName;
            updateUser.Address = user.Address;
            updateUser.PostCode = user.PostCode;

            var result = await userManager.UpdateAsync(updateUser);
            if (!result.Succeeded)
            {
                return null;
            }
            return updateUser;
        }
    }
}
